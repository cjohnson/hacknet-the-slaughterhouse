# Hacknet - The Slaughterhouse #

In The Slaughterhouse you break into the computer system of an abattoir to expose its violations to the public.

This extension takes approximately 2 hours to play. You need to be familiar with tools used in both Hacknet and the Labyrinths DLC.

### Steam Users ###

You can download this extension using Steam Workshop. [Click here](http://steamcommunity.com/sharedfiles/filedetails/?id=930832695) to view this extension in Steam Workshop.

### Non-Steam Users ###

Download the contents of this repository into the `Extensions/TheSlaughterhouse` folder. This extension should then appear in the extensions menu.

If you are not familiar with git then just got to the Downloads section to download this repository.